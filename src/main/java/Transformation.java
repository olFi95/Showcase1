import Jama.Matrix;

public abstract class Transformation{
    protected Matrix mat;
    public Transformation() {

    }

    public abstract Matrix apply(Matrix matrix);
}
