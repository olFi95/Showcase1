public class main {
    public static void main(String[] args) {
        new main();
    }
    public main() {
        Controller controller = new Controller();
        Long timeBevore = System.currentTimeMillis();
        Long timeAfter = timeBevore;
        while(true){
            timeBevore = System.currentTimeMillis();
            controller.update((timeAfter - timeBevore) / 1000.0);
            timeAfter = System.currentTimeMillis();
            try {
                Thread.sleep(16);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
