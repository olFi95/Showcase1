import Jama.Matrix;

public class point2D {
    private Matrix matrix;
    public point2D(double x, double y){
        double [][] mat = {{x},{y}};
        matrix = new Matrix(mat);
    }

    public Matrix getMatrix() {
        return matrix;
    }

    public void setMatrix(Matrix matrix) {
        this.matrix = matrix;
    }

    public int getX(){
        return (int)matrix.get(0,0);
    }
    public int getY(){
        return (int)matrix.get(1,0);
    }
}
