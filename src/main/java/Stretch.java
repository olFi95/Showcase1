import Jama.Matrix;

public class Stretch extends Transformation {
    private double x,y;


    public Stretch(double x, double y){
            this.x=x;
            this.y=y;
            updateMatrix();
    }

    @Override
    public Matrix apply(Matrix matrix) {
        return this.mat.times(matrix);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
        updateMatrix();
    }

    private void updateMatrix() {
        double[][] tmp = {{x,0},{0,y}};
        this.mat = new Matrix(tmp);
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
        updateMatrix();
    }
}
