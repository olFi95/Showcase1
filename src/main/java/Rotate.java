import Jama.Matrix;

public class Rotate extends Transformation {
    private double angle;
    public Rotate(double angle) {
        this.angle = angle;
        buildMatrix();
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
        buildMatrix();
    }

    public void addAngle(double angle) {
        this.angle = (this.angle + angle) % 360;
        buildMatrix();
    }

    private void buildMatrix(){
        double[][] tempmat ={{   Math.cos(angle), -Math.sin(angle)},
                                {Math.sin(angle), Math.cos(angle)}};
        this.mat = new Matrix(tempmat);
    }

    @Override
    public Matrix apply(Matrix matrix) {
        Matrix a = Matrix.constructWithCopy(this.mat.getArray());
        return a.times(matrix);
    }
}
