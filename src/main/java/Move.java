import Jama.Matrix;

public class Move extends Transformation {
    private double x,y;


    public Move(double x, double y){
            this.x=x;
            this.y=y;
            updateMatrix();
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
        updateMatrix();
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
        updateMatrix();
    }

    private void updateMatrix() {
        double[][] tmp = {{x}, {y}};
        this.mat = new Matrix(tmp);
    }

    @Override
    public Matrix apply(Matrix matrix) {
        return this.mat.plus(matrix);
    }
}
