import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Controller extends JPanel {
    private List<Geomerty> geoms = new ArrayList();
    private JFrame frame = new JFrame();
    private Rotate rot;
    private Move mv;
    private double time = 0;


    public Controller(){
        initGui();
        Geomerty rect = new Geomerty();
        rect.addPoint(new point2D(-100,100));
        rect.addPoint(new point2D(100,100));
        rect.addPoint(new point2D(100,-100));
        rect.addPoint(new point2D(-100,-100));
        geoms.add(rect);
        rot = new Rotate(0);
        rect.addTransformation(rot);
        mv = new Move(250, 250);
        rect.addTransformation(mv);
    }

    private void initGui() {
        frame.add(this);
        frame.setPreferredSize(new Dimension(800, 800));
        frame.pack();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
        setDoubleBuffered(true);
    }

    public void update(double seconds) {
        rot.addAngle(-seconds * 2);
        time = (time + seconds) % (2 * Math.PI);
        mv.setX(400 + Math.sin(time) * 250);
        mv.setY(375 + Math.cos(time) * 250);
        this.repaint();
    }

    public void paintComponent(Graphics gr) {
        super.paintComponent(gr);
        Graphics2D g = (Graphics2D) gr;
        for (Geomerty geom :
                geoms) {
            geom.executeTransformations();
            geom.draw(g);
        }
    }
}
