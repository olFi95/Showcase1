import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Geomerty {
    private List<point2D> origins = new ArrayList<point2D>();
    private List<point2D> temp = new ArrayList<point2D>();
    private List<Transformation> transformations = new ArrayList<Transformation>();
    public void executeTransformations() {
        temp = new ArrayList<>();
        for (point2D point : origins) {
            point2D p = new point2D(point.getX(),point.getY());
            temp.add(p);
            for (Transformation trans : transformations) {
                p.setMatrix(trans.apply(p.getMatrix()));
            }
        }
    }

    public void addPoint(point2D p){
        origins.add(p);
    }

    public void addTransformation(Transformation transformation){
        transformations.add(transformation);
    }

    public void draw(Graphics2D g) {
        if (temp.size()<2)
            return;
        for (int i = 0; i < temp.size() - 1; i++) {
            g.drawLine(temp.get(i).getX(), temp.get(i).getY(), temp.get(i+1).getX(), temp.get(i+1).getY());
        }
        g.drawLine(temp.get(temp.size() - 1).getX(), temp.get(temp.size() - 1 ).getY(), temp.get(0).getX(), temp.get(0).getY());
    }
}
